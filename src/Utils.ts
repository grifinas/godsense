'use strict';

export function objPrint(stuff) {
    for(let i in stuff) {
        console.log(i, stuff[i]);
    }
}
    
export function reverse(s : string) {
    return s.split("").reverse().join('');
}