'use strict';

import * as vscode from 'vscode';
import * as utils from './Utils';

export default class Peeker {
    peekDef () : any {
        var editor = vscode.window.activeTextEditor;
        if (!editor) {
            return; // No open text editor
        }

        var selection = editor.selection;
        var text = editor.document.lineAt(selection.start);

        // Display a message box to the user
        
        let a = this.getFunctionName(selection.start, text);
        vscode.window.showInformationMessage('Line: ' + text.text + ' fn: ' + a);
    }

    getFunctionName(pos, line) : string {  
        const function_name_reg = /[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*/;
        
        let working_str = line.text;
        for (var i = pos.character; i--;) {
            if (!working_str[i].match(function_name_reg)) {
                //current char is bad go to piev one
                i++;
                break;
            }
        }

        for (var j = i; j < working_str.length; j++) {
            if (!working_str[j].match(function_name_reg)) {
                break;
            }
        }

        var fn = line.text.substring(i, j);

        let match = fn.match(function_name_reg);
        if (!match || match[0] != fn) {
            vscode.window.showInformationMessage(fn + ' is not recognised as a function');
            throw Error('shit getFnName');
        }

        return fn;
    }
}

